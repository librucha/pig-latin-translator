package org.librucha.sapho.piglatintranslator.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.ObjectArrayArguments.create;

class TokenUtilsTest {

    private static Stream<Arguments> punctuationStringProvider() {
        return Stream.of(
                create(null, null, null),
                create("", "", ""),
                create(" ", " ", " "),
                create("\n      \t", "\n      \t", "\n      \t"),
                create("can't", "an'tcay", "antca'y"),
                create(":yes", "say", ":say"),
                create("holybe;ans", "nasbe", "na;sbe"),
                create("holybeans;", "nasbe", "nasbe;")
        );
    }

    @ParameterizedTest(name = "[{index}] When process origin=''{0}'', result=''{1}'' then return ''{2}''")
    @MethodSource(names = {"punctuationStringProvider"})
    @DisplayName("Preserve Punctuation test")
    void preservePunctuation(String origin, String result, String expectedResult) {
        String actualResult = TokenUtils.preservePunctuation(origin, result);

        assertThat(actualResult).isEqualTo(expectedResult);
    }

    private static Stream<Arguments> caseStringProvider() {
        return Stream.of(
                create(null, null, null),
                create("", "", ""),
                create(" ", " ", " "),
                create("\n      \t", "\n      \t", "\n      \t"),
                create("Beach", "eachBay", "Eachbay"),
                create("McCloud", "cCloudMay", "CcLoudmay"),
                create("McClouD", "cClouDMay", "CcLoudMay")
        );
    }

    @ParameterizedTest(name = "[{index}] When process origin=''{0}'', result=''{1}'' then return ''{2}''")
    @MethodSource(names = {"caseStringProvider"})
    @DisplayName("Preserve Case test")
    void preserveCase(String origin, String result, String expectedResult) {
        String actualResult = TokenUtils.preserveCase(origin, result);

        assertThat(actualResult).isEqualTo(expectedResult);
    }
}