package org.librucha.sapho.piglatintranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.librucha.sapho.piglatintranslator.rule.RuleChain;
import org.librucha.sapho.piglatintranslator.token.TokenInfo;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class TranslatorTokenizerTest {

    @InjectMocks
    private Translator translator;
    @Mock
    private RuleChain ruleChain;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        this.translator = new Translator(ruleChain);
    }

    @Test
    @DisplayName("When translate sentence then sentence is tokenize correctly")
    void tokenize() {
        doReturn("").when(ruleChain).process(any());

        translator.translate("  Don't   be \tlike Foo-Bar\npeople.");

        verify(ruleChain).process(new TokenInfo("Don't"));
        verify(ruleChain).process(new TokenInfo("be"));
        verify(ruleChain).process(new TokenInfo("like"));
        verify(ruleChain).process(new TokenInfo("Foo"));
        verify(ruleChain).process(new TokenInfo("Bar"));
        verify(ruleChain).process(new TokenInfo("people"));

        verifyNoMoreInteractions(ruleChain);
    }

    @Test
    @DisplayName("When translate sentence then delimiter positions are preserved")
    void preserveNonTokens() {
        doReturn("x").when(ruleChain).process(any());

        String result = translator.translate("  Don't   be \tlike Foo-Bar\npeople.");

        assertThat(result)
                .isNotNull()
                .isNotEmpty()
                .isEqualTo("  x   x \tx x-x\nx.");
    }
}