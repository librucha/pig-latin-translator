package org.librucha.sapho.piglatintranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.librucha.sapho.piglatintranslator.rule.EndingCharacterRule;
import org.librucha.sapho.piglatintranslator.rule.RuleChain;
import org.librucha.sapho.piglatintranslator.rule.StartingCharacterRule;

import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.ObjectArrayArguments.create;

public class TranslatorTest {

    private Translator translator;

    @BeforeEach
    void setUp() {
        translator = new Translator(new RuleChain(Stream.of(
                new EndingCharacterRule(),
                new StartingCharacterRule()
        ).collect(toList())));
    }

    private static Stream<Arguments> translationFeedProvider() {
        return Stream.of(
                create(null, null),
                create("", ""),
                create(" ", " "),
                create("\n      \t", "\n      \t"),
                create("Hello", "Ellohay"),
                create("apple", "appleway"),
                create("stairway", "stairway"),
                create("can't", "antca'y"),
                create("end.", "endway."),
                create("this-thing", "histay-hingtay"),
                create("Beach", "Eachbay"),
                create("McCloud", "CcLoudmay")
        );
    }

    @ParameterizedTest(name = "[{index}] When translate ''{0}'' then return ''{1}''")
    @MethodSource(names = {"translationFeedProvider"})
    @DisplayName("Translation test")
    void translate(String text, String expectedResult) {
        String result = translator.translate(text);

        assertThat(result).isEqualTo(expectedResult);
    }
}