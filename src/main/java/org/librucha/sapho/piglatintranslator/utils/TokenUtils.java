package org.librucha.sapho.piglatintranslator.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public final class TokenUtils {

    private static final Set<Character> PUNCTUATION = new HashSet<>(Arrays.asList(',', ':', '\'', ';', '’'));
    private static final String PUNCTUATION_REGEX = PUNCTUATION.stream().map(String::valueOf).collect(Collectors.joining("|"));

    private TokenUtils() {
        throw new AssertionError("No " + getClass().getName() + " instances for you!");
    }

    public static String preservePunctuation(final String origin, final String result) {
        if (StringUtils.isBlank(origin)) {
            return result;
        }
        char[] originChars = origin.toCharArray();
        String normalizedResult = StringUtils.removeAll(result, PUNCTUATION_REGEX);
        for (int i = 0; i < originChars.length; i++) {
            char originChar = originChars[i];
            if (PUNCTUATION.contains(originChar)) {
                int originEndDistance = originChars.length - i;
                int resultEndIndex = normalizedResult.length() - originEndDistance + 1;
                normalizedResult = normalizedResult.substring(0, resultEndIndex) + originChar + normalizedResult.substring(resultEndIndex);
            }
        }
        return normalizedResult;
    }

    public static String preserveCase(final String origin, final String result) {
        if (StringUtils.isBlank(origin)) {
            return result;
        }
        char[] originChars = origin.toCharArray();
        char[] resultChars = result.toCharArray();
        for (int i = 0; i < origin.length(); i++) {
            char originChar = originChars[i];
            if (Character.isUpperCase(originChar)) {
                resultChars[i] = Character.toUpperCase(resultChars[i]);
            } else {
                resultChars[i] = Character.toLowerCase(resultChars[i]);
            }
        }
        return new String(resultChars);
    }
}
