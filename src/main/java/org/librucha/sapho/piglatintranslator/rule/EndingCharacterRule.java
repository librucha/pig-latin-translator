package org.librucha.sapho.piglatintranslator.rule;

import org.librucha.sapho.piglatintranslator.token.TokenInfo;

public class EndingCharacterRule implements Rule {

    @Override
    public boolean run(TokenInfo tokenInfo) {
        return !tokenInfo.getOrigin().endsWith("way");
    }
}
