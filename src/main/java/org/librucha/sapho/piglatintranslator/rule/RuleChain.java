package org.librucha.sapho.piglatintranslator.rule;

import org.librucha.sapho.piglatintranslator.token.TokenInfo;

import java.util.List;

import static java.util.Collections.emptyList;

public class RuleChain {

    private final List<Rule> rules;

    public RuleChain(List<Rule> rules) {
        if (rules == null) {
            this.rules = emptyList();
        } else {
            this.rules = rules;
        }
    }

    public String process(TokenInfo tokenInfo) {
        for (Rule rule : rules) {
            boolean shouldContinue = rule.run(tokenInfo);
            if (!shouldContinue) {
                return tokenInfo.getResult();
            }
        }
        return tokenInfo.getResult();
    }
}
