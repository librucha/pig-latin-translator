package org.librucha.sapho.piglatintranslator.rule;

import org.librucha.sapho.piglatintranslator.token.TokenInfo;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.librucha.sapho.piglatintranslator.utils.TokenUtils.preserveCase;
import static org.librucha.sapho.piglatintranslator.utils.TokenUtils.preservePunctuation;

public class StartingCharacterRule implements Rule {

    private static final Set<Character> VOWELS = new HashSet<>(Arrays.asList('a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U'));

    @Override
    public boolean run(TokenInfo tokenInfo) {
        String token = tokenInfo.getOrigin();
        if (VOWELS.contains(token.charAt(0))) {
            tokenInfo.setResult(token + "way");
            return false;
        } else {
            String result = token.substring(1) + token.charAt(0) + "ay";
            tokenInfo.setResult(preserveCase(token, preservePunctuation(token, result)));
            return false;
        }
    }
}
