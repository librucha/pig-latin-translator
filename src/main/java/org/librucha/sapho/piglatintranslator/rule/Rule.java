package org.librucha.sapho.piglatintranslator.rule;

import org.librucha.sapho.piglatintranslator.token.TokenInfo;

public interface Rule {

    /**
     * Processing one tokenInfo.
     *
     * @param tokenInfo current wrapped token
     * @return {@code true} for continue next rule or {@code false} for stop processing
     */
    boolean run(TokenInfo tokenInfo);
}
