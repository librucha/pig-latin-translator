package org.librucha.sapho.piglatintranslator.token;

import lombok.Data;

@Data
public class TokenInfo {

    public TokenInfo(String origin) {
        this.origin = origin;
        this.result = origin;
    }

    private final String origin;
    private String result;
    private boolean finished;
}
