package org.librucha.sapho.piglatintranslator;

import org.apache.commons.lang3.StringUtils;
import org.librucha.sapho.piglatintranslator.rule.RuleChain;
import org.librucha.sapho.piglatintranslator.token.TokenInfo;

import java.util.*;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;

public class Translator {

    private static final Set<Character> TOKEN_DELIMITERS = new HashSet<>(Arrays.asList(' ', '\t', '\n', '\r', '\f', '-', '.'));
    private static String DELIMITERS = TOKEN_DELIMITERS.stream().map(String::valueOf).collect(joining());

    private final RuleChain ruleChain;

    public Translator(RuleChain ruleChain) {
        this.ruleChain = requireNonNull(ruleChain, "ruleChain must not be null");
    }

    public String translate(String text) {
        if (StringUtils.isBlank(text)) {
            return text;
        }

        StringTokenizer tokenizer = new StringTokenizer(text, DELIMITERS, true);
        StringBuilder result = new StringBuilder();
        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            if (TOKEN_DELIMITERS.contains(token.charAt(0))) {
                result.append(token);
            } else {
                result.append(processToken(token));
            }
        }

        return result.toString();
    }

    private String processToken(String token) {
        return ruleChain.process(new TokenInfo(token));
    }
}
